# mom

#### Description
MOM is a policy-driven tool that can be used to manage overcommitment on KVM
hosts.  Using a connection to the hypervisor software (either libvirt or oVirt
vdsm), MOM keeps track of active virtual machines on a host.  At a regular
collection interval, data is gathered about the host and guests. Data can come
from multiple sources (eg. the /proc interface, libvirt API calls, a client
program connected to a guest, etc). Once collected, the data is organized for
use by the policy evaluation engine.  If configured, MOM regularly evaluates a
user-supplied management policy using the collected data as input.  In response
to certain conditions, the policy may trigger reconfiguration of the system’s
overcommitment mechanisms. Currently MOM supports control of memory ballooning
and KSM but the architecture is designed to accommodate new mechanisms such as
cgroups.

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
